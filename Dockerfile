FROM python:3.6

RUN mkdir new_chatapp
copy ./new_chatapp/ /new_chatapp

WORKDIR /new_chatapp
RUN pip3 install -r requirements.txt

EXPOSE 8000
WORKDIR /new_chatapp/fundoo
RUN chmod +x run.sh

CMD ["./run.sh"]
